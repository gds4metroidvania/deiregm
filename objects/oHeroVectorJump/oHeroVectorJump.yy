{
    "id": "39dce505-952a-423f-8172-a168fa81c7d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHeroVectorJump",
    "eventList": [
        {
            "id": "a6d2778c-a6ae-4ca5-9573-4e105408358f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "39dce505-952a-423f-8172-a168fa81c7d7"
        },
        {
            "id": "3147b5cc-b790-462b-bb5f-1f621914c2aa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "39dce505-952a-423f-8172-a168fa81c7d7"
        }
    ],
    "maskSpriteId": "243226ea-20cd-43ce-84d6-9e0080a8e8a9",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "6b8dbfc7-73b1-41d4-9721-8b762a010b54",
    "visible": true
}