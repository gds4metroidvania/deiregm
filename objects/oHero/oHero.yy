{
    "id": "03e23ac4-1f55-4fe3-93d1-44c4ef0f2570",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oHero",
    "eventList": [
        {
            "id": "2d55c08f-665f-4e57-8814-56732f75009a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "03e23ac4-1f55-4fe3-93d1-44c4ef0f2570"
        },
        {
            "id": "66de8bce-8fc7-4a72-8519-00164fccf974",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "03e23ac4-1f55-4fe3-93d1-44c4ef0f2570"
        },
        {
            "id": "b526856e-e815-44d7-be34-1e9c584d8c8f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "03e23ac4-1f55-4fe3-93d1-44c4ef0f2570"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": [
        {
            "id": "209eb6a4-d873-487a-a928-4563d022c291",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 0
        },
        {
            "id": "23bfc2fe-bee4-4103-bce6-dc131adc71af",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 470,
            "y": 0
        },
        {
            "id": "6e49c5b4-6296-4769-af4a-158c768c5d49",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 470,
            "y": 869
        },
        {
            "id": "26bebd86-61d8-4ff8-9334-7e5ffb692aaa",
            "modelName": "GMPoint",
            "mvc": "1.0",
            "x": 0,
            "y": 869
        }
    ],
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "243226ea-20cd-43ce-84d6-9e0080a8e8a9",
    "visible": true
}