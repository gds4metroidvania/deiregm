/// @description init
grav=0.40;
horizontalSpeed=4.2;
grounded=false;
holdJumpMax=5;
holdJump=0;
vectorMovement=false;
var _colliderBorder=2;
var _upCollider=collision_rectangle(bbox_left,bbox_top,bbox_right,bbox_top+_colliderBorder,oBlock,false,true);
var _downCollider=collision_rectangle(bbox_left,bbox_bottom,bbox_right,bbox_bottom-_colliderBorder,oBlock,false,true);

//ver1

if scrRaycast(100,bbox_top,-1)==0 && scrRaycast(100,bbox_bottom,-1)<sprite_height
	y+=sprite_height-scrRaycast(100,bbox_bottom,-1);

if scrRaycast(100,bbox_bottom,1)==0 && scrRaycast(100,bbox_top,1)<sprite_height
	y-=sprite_height-scrRaycast(100,bbox_top,1);

if scrRaycastH(100,bbox_right,1)==0 && scrRaycastH(100,bbox_left,1)<sprite_width
	x-=sprite_width-scrRaycastH(100,bbox_left,1);

if scrRaycastH(100,bbox_left,-1)==0 && scrRaycastH(100,bbox_right,-1)<sprite_width
	x+=sprite_width-scrRaycastH(100,bbox_right,-1);