/// @description Insert description here
// You can write your code in this editor
var _maxVspeed=30;
var _jumpPower=10;
var _haxis=(keyboard_check(ord("D")) - keyboard_check(ord("A")))*horizontalSpeed;
var _jumpButton=vk_space;

//physics
#region
	if(!place_meeting(x,y+vspeed,oBlock)){
		vspeed+=grav;
		grounded=false;		
	}
	else if(vspeed>0){
		move_contact_solid(270,-1);
		vspeed=0;
		grounded=true;
		holdJump=0;
	}
	else if(vspeed<0){
		vspeed*=-1;		
	}	
	
	if(grounded && keyboard_check_pressed(_jumpButton)){
		vspeed=-_jumpPower;
	}
	
	if(!grounded && keyboard_check(_jumpButton) && holdJump<holdJumpMax){
		vspeed=-_jumpPower;
		holdJump+=0.3;
	}
	
	if(!grounded && !keyboard_check(_jumpButton))
		holdJump=holdJumpMax;
		
	vspeed=clamp(vspeed,-50,_maxVspeed);
	hspeed=_haxis;
	
#endregion

//vector
#region
	
	if(mouse_check_button(mb_left)){
		vectorMovement=true;	
	}
	else{
		if(vectorMovement){
			(instance_create_layer(x,y,"Instances",oHeroVectorJump)).parent=id;
			vectorMovement=false;
			instance_destroy();
		}
			
	}
	
#endregion
