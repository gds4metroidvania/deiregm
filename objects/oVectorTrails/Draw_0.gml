/// @description Insert description here
// You can write your code in this editor
if (instance_exists(oHero) && oHero.vectorMovement){
	var _marginLenght=13;
	
	px=oHero.x;
	py=oHero.y;
	grav=oHero.grav;
	margin=_marginLenght;
	i=0;
	
	direction=point_direction(px,py,mouse_x,mouse_y);
		
	while(i<20){
		px+=lengthdir_x(3,direction);
		py+=lengthdir_y(3,direction);
		
		margin--;
		
		if(margin=0){		
			draw_sprite(sVectorTrail,0,px,py);
			margin=_marginLenght;
			i++;
		}
		
		if(position_meeting(px,py,oBlock))
			break;		
				
	}
	
}
