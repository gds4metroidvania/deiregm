{
    "id": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeroJumpLanding",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 876,
    "bbox_left": 24,
    "bbox_right": 723,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "015b8be3-0908-427a-8e4f-93bee324d93b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "0adf8598-2021-41bf-b239-13a619b8f53c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "015b8be3-0908-427a-8e4f-93bee324d93b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1afb2333-7b64-44a4-883e-dbedadc1e475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "015b8be3-0908-427a-8e4f-93bee324d93b",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "7fbf87a8-ae14-475b-b0cb-62509c8d7346",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "76c38c1a-4fc8-48ab-af2b-8c49f51ecfb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7fbf87a8-ae14-475b-b0cb-62509c8d7346",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ea34b46-51c9-4744-86ee-53bb3c7fb05c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7fbf87a8-ae14-475b-b0cb-62509c8d7346",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "5d33f653-4e0d-4528-9c2d-e0241ca239d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "beda413c-a7ae-4e4f-bca6-43c5071757e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5d33f653-4e0d-4528-9c2d-e0241ca239d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3466b8b2-577d-40b3-ae48-b499042c3d5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5d33f653-4e0d-4528-9c2d-e0241ca239d4",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "da5ebc72-1ea6-4811-964b-c39da8999a18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "aaca4af8-f480-446f-9698-f3f33a6091ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "da5ebc72-1ea6-4811-964b-c39da8999a18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e9eb3067-9e85-4589-880a-bc8fe121a5d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "da5ebc72-1ea6-4811-964b-c39da8999a18",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "34249118-dc6c-4efd-9a53-627f77f9cecc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "262de18c-4b3b-4406-9a4f-26d57ba0e839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34249118-dc6c-4efd-9a53-627f77f9cecc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcd522fc-6217-4c13-bf57-3d73b75eed3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34249118-dc6c-4efd-9a53-627f77f9cecc",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "947282b1-12fa-4267-93bd-8bfac23d33a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "799aebc2-3c31-4423-a40a-6a13b20f21ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "947282b1-12fa-4267-93bd-8bfac23d33a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6a8cd194-646e-4ab2-b968-64a0e7ae2f0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "947282b1-12fa-4267-93bd-8bfac23d33a0",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "4ee03960-b255-46e7-afcd-5f4d52b239f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "d0af5107-5f45-48b3-958b-255e2b34b99c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee03960-b255-46e7-afcd-5f4d52b239f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b33b25be-9273-4b2c-800e-e02fd0b41216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee03960-b255-46e7-afcd-5f4d52b239f5",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "4c9bc889-0381-4351-814f-6606dad94018",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "1cf06158-6cc8-4dc8-842c-cab98e37d757",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c9bc889-0381-4351-814f-6606dad94018",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "40bc0012-208a-4fab-8f10-00ed9787cdfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c9bc889-0381-4351-814f-6606dad94018",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "6a1c60be-f073-4f65-86f0-f04a9f5bbf17",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "928b06db-966a-4129-b502-0c9aa3cbd9af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a1c60be-f073-4f65-86f0-f04a9f5bbf17",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d272609d-6199-4bfe-b573-a044b9a85cfb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a1c60be-f073-4f65-86f0-f04a9f5bbf17",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "d3d7f74b-23af-4722-9b97-7ba430bac232",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "6edf3a64-8137-4d3b-bf9e-9a52c85bf8f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3d7f74b-23af-4722-9b97-7ba430bac232",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66c4ab2e-7b9b-4256-b79d-2332e2ec60ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3d7f74b-23af-4722-9b97-7ba430bac232",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "42b8d4de-b45b-441a-a29e-1a0cedfd2de3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "3975ce43-310a-4975-8a3e-ea6e193d14c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "42b8d4de-b45b-441a-a29e-1a0cedfd2de3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a83d5ba-b7b0-473f-b1e0-652f1f01d851",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "42b8d4de-b45b-441a-a29e-1a0cedfd2de3",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "f07ec10d-ed86-4179-aeea-04fa29ff514d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "69176919-5d19-4405-ba06-425876f812e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f07ec10d-ed86-4179-aeea-04fa29ff514d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c240bbb2-8821-45da-a0b4-01fa4de3b864",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f07ec10d-ed86-4179-aeea-04fa29ff514d",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "52b84c82-d388-4380-96c2-93bb8c450732",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "b65d18d2-a118-49d5-8cb6-fceb665586d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52b84c82-d388-4380-96c2-93bb8c450732",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b161c489-3a92-468a-943e-9f85de039174",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52b84c82-d388-4380-96c2-93bb8c450732",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "d1f26a65-7e41-4375-8379-c05b4496d05f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "75a16c7d-29c2-4d1e-be4f-9592cf926a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1f26a65-7e41-4375-8379-c05b4496d05f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d2a8b7d-4044-4c7b-b1cf-5ae5337236d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1f26a65-7e41-4375-8379-c05b4496d05f",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "908a5f50-f39f-4658-b875-d73ab8007450",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "6b765819-c4a2-4dcb-8f54-1679029f1a96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "908a5f50-f39f-4658-b875-d73ab8007450",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb2c14ca-0017-419e-8f0f-48ac9daddb27",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "908a5f50-f39f-4658-b875-d73ab8007450",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "2d69ef71-1829-4719-85cf-36e17dfb8fa6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "41ce98d1-8374-4740-822c-67d89e8623bb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d69ef71-1829-4719-85cf-36e17dfb8fa6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c72471a-efa9-4362-9984-14a6ebcbdb0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d69ef71-1829-4719-85cf-36e17dfb8fa6",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        },
        {
            "id": "136af5e9-e7e2-48e8-b653-610aa0bda9c3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "compositeImage": {
                "id": "7de34f6d-87b1-45ab-ad70-a6dd4d311d6d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "136af5e9-e7e2-48e8-b653-610aa0bda9c3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a138ac18-838b-43cf-a013-5ec518c29a9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "136af5e9-e7e2-48e8-b653-610aa0bda9c3",
                    "LayerId": "330bd3c6-d3e4-4969-8399-79fcfb01cccc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 885,
    "layers": [
        {
            "id": "330bd3c6-d3e4-4969-8399-79fcfb01cccc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1922c2fe-a932-4b02-912c-db94f7c28aa6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 752,
    "xorig": 376,
    "yorig": 442
}