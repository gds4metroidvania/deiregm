{
    "id": "479a93bc-3624-4787-a501-5d70ba73b49a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeroJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 875,
    "bbox_left": 31,
    "bbox_right": 720,
    "bbox_top": 99,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9dc79a29-9520-4677-8888-5d30077187b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "479a93bc-3624-4787-a501-5d70ba73b49a",
            "compositeImage": {
                "id": "152b6823-048d-4878-b70a-f4781c6b4c23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dc79a29-9520-4677-8888-5d30077187b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9af4d296-665b-41b7-9a7b-1a70c577982c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dc79a29-9520-4677-8888-5d30077187b5",
                    "LayerId": "43ad4760-a58f-411d-bdc5-feec3ede4109"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 885,
    "layers": [
        {
            "id": "43ad4760-a58f-411d-bdc5-feec3ede4109",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "479a93bc-3624-4787-a501-5d70ba73b49a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 752,
    "xorig": 376,
    "yorig": 442
}