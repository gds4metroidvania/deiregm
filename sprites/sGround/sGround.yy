{
    "id": "c554cc6d-dee2-45d1-8698-d813c8841d95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGround",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "22d56d2f-34a8-4d0f-8a60-590a4f7c4ee2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c554cc6d-dee2-45d1-8698-d813c8841d95",
            "compositeImage": {
                "id": "dc63b7e4-68ba-424f-8841-c0716ab851f9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "22d56d2f-34a8-4d0f-8a60-590a4f7c4ee2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64f41986-a356-4fae-b647-91d6e001a875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "22d56d2f-34a8-4d0f-8a60-590a4f7c4ee2",
                    "LayerId": "0f533d22-d9e4-4954-b083-fbe52acdde8f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0f533d22-d9e4-4954-b083-fbe52acdde8f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c554cc6d-dee2-45d1-8698-d813c8841d95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}