{
    "id": "4389e593-af47-4035-a677-b1ecbea9a3cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeroRun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 873,
    "bbox_left": 8,
    "bbox_right": 841,
    "bbox_top": 43,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "63afff11-d4f5-4e9a-a4b7-8580751e7f4b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "1210bea0-26d6-461b-b92d-bdabf1095b1e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63afff11-d4f5-4e9a-a4b7-8580751e7f4b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3736c99a-6154-4093-8789-a0abfb77d8e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63afff11-d4f5-4e9a-a4b7-8580751e7f4b",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "8f4cd74a-fab5-45a8-93e0-5f39fb521cdd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "06426a12-5991-4d00-99e8-f889906057b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f4cd74a-fab5-45a8-93e0-5f39fb521cdd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b69367d4-409f-4866-8232-c23eda9a37d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f4cd74a-fab5-45a8-93e0-5f39fb521cdd",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "899e4b36-1137-4319-9aaf-9fbdc08836bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "559a136e-41e3-4ffe-a58d-4840e0087119",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "899e4b36-1137-4319-9aaf-9fbdc08836bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25f715a8-e0cb-4728-a23b-31e274f2c018",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "899e4b36-1137-4319-9aaf-9fbdc08836bd",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "3af3f825-11bd-4247-a036-b71b30c5f8a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "42fb222d-6ffc-4464-a5b5-36813072a20e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3af3f825-11bd-4247-a036-b71b30c5f8a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c010364f-513c-4b63-b819-ee4b058cfaeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3af3f825-11bd-4247-a036-b71b30c5f8a5",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "89d8c39b-1062-4649-92b6-e8d07c22676b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "fdf06486-3615-4467-8187-3814f55906c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89d8c39b-1062-4649-92b6-e8d07c22676b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "162135bf-7201-4cbb-996d-80a17508bc0e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89d8c39b-1062-4649-92b6-e8d07c22676b",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "4651b9d8-706d-4bfc-bf10-4b27fb4bdf9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "18119255-bac7-4ea9-a3d2-1b5651b7f60c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4651b9d8-706d-4bfc-bf10-4b27fb4bdf9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69607f18-14e1-43e7-a6a7-d611ec12e072",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4651b9d8-706d-4bfc-bf10-4b27fb4bdf9e",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "f1c140b7-a578-4e86-b0ab-d5ff3a1be226",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "cabba572-4a91-43b6-abab-e37613952dc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f1c140b7-a578-4e86-b0ab-d5ff3a1be226",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8607cb70-5229-48ad-a98a-227255384c0d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f1c140b7-a578-4e86-b0ab-d5ff3a1be226",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "761ad16f-a608-40b0-b1df-661ca99f1327",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "be29efb9-c5bc-4ea0-a229-2dfd96c53a01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "761ad16f-a608-40b0-b1df-661ca99f1327",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f23f7ea-4fde-4129-aec9-d10283e9b616",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "761ad16f-a608-40b0-b1df-661ca99f1327",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "fa97d1a3-3660-4a6b-b377-2f4424880253",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "cb8586cf-075d-40e2-8297-50704073a7a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa97d1a3-3660-4a6b-b377-2f4424880253",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5fe42b0-130e-4b6a-b668-e3496b061f3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa97d1a3-3660-4a6b-b377-2f4424880253",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "7c836eb0-6c51-4741-8734-607c4dad8275",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "6e0cb5af-f2c1-4a09-b63a-da8f2d6534ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c836eb0-6c51-4741-8734-607c4dad8275",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a5e096c-0a6b-4648-8477-09beaf23178f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c836eb0-6c51-4741-8734-607c4dad8275",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "7595517f-5270-4469-802c-c9ba15f8e870",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "0b0e3bfb-c910-4e3d-906f-9c37831229d3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7595517f-5270-4469-802c-c9ba15f8e870",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "46a67bf6-3b53-4d46-b3f0-d12cb420a085",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7595517f-5270-4469-802c-c9ba15f8e870",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "d43f215e-5315-4bbf-8cd1-893cbce8aac7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "6c86b4cc-8736-46f9-9f73-5e9ec8404392",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d43f215e-5315-4bbf-8cd1-893cbce8aac7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff52f018-3267-41a7-a167-f5206014c1ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d43f215e-5315-4bbf-8cd1-893cbce8aac7",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "87bb7d88-6ff6-447c-a9cd-8df100391424",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "a412777c-7828-4945-bc67-fbc16716efc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bb7d88-6ff6-447c-a9cd-8df100391424",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e01b3a-f064-490f-87c2-d639527e8732",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bb7d88-6ff6-447c-a9cd-8df100391424",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "73be51c5-1797-4d44-9584-6f2644976997",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "3a6dc05c-b162-43eb-bdfe-a7b79b35fc51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73be51c5-1797-4d44-9584-6f2644976997",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a1f7481-56b6-4e5a-84f1-19609ed4896c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73be51c5-1797-4d44-9584-6f2644976997",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "3afa635d-be87-4fe3-9bf8-572f4d755da9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "4afe2f96-01d2-42f4-ba86-43e56f36634f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3afa635d-be87-4fe3-9bf8-572f4d755da9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80b6ed99-0e85-4c94-9b8f-af7fdaf5a182",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3afa635d-be87-4fe3-9bf8-572f4d755da9",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "33fc0256-b5d3-42ba-a437-b6c75cb39576",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "c61983ad-742f-44b5-adb8-3203e6890d14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "33fc0256-b5d3-42ba-a437-b6c75cb39576",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ed7c38f-380b-4ed8-bc3b-0b6ee357ba48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "33fc0256-b5d3-42ba-a437-b6c75cb39576",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "9b004c21-2392-4c21-af70-6eabf1e7386d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "45be6a1f-b38d-432b-8c7e-a396169e9d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b004c21-2392-4c21-af70-6eabf1e7386d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ac4a998-6f26-4f1b-b4b2-18ca6a81a1e2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b004c21-2392-4c21-af70-6eabf1e7386d",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "a70e5092-a493-4f13-9e51-bfdf419e347b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "bf188492-c958-441c-886b-ceaee943f2bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a70e5092-a493-4f13-9e51-bfdf419e347b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b2d8286-25d8-4298-be02-da3a1ace63e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a70e5092-a493-4f13-9e51-bfdf419e347b",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        },
        {
            "id": "b2fc5863-faa9-4faf-b8fb-ee7216e6ce25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "compositeImage": {
                "id": "4106103e-446b-47ed-a31f-ab50453df02c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2fc5863-faa9-4faf-b8fb-ee7216e6ce25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f097285-9e2c-4725-b1a2-e7169f93e7e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2fc5863-faa9-4faf-b8fb-ee7216e6ce25",
                    "LayerId": "9465973b-e3f8-4386-869a-00e2cf1df371"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 876,
    "layers": [
        {
            "id": "9465973b-e3f8-4386-869a-00e2cf1df371",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4389e593-af47-4035-a677-b1ecbea9a3cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 1,
    "playbackSpeedType": 1,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 864,
    "xorig": 432,
    "yorig": 438
}