{
    "id": "243226ea-20cd-43ce-84d6-9e0080a8e8a9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeroIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 88,
    "bbox_left": 0,
    "bbox_right": 52,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "d1e24969-d92b-40e9-98ed-8047eccf0274",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "243226ea-20cd-43ce-84d6-9e0080a8e8a9",
            "compositeImage": {
                "id": "e27ff6fb-4780-4b10-a85f-d475cfbfea1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1e24969-d92b-40e9-98ed-8047eccf0274",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b708fe8c-243b-4d59-bed1-9d076c9e675b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1e24969-d92b-40e9-98ed-8047eccf0274",
                    "LayerId": "238d8c94-3866-453e-9be8-8c60a2047a4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 89,
    "layers": [
        {
            "id": "238d8c94-3866-453e-9be8-8c60a2047a4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "243226ea-20cd-43ce-84d6-9e0080a8e8a9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 53,
    "xorig": 26,
    "yorig": 44
}