{
    "id": "beed4201-d470-4875-8fa2-96d7b549223e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCollmask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "23eef149-99f9-45b8-ad9e-23859d370142",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "beed4201-d470-4875-8fa2-96d7b549223e",
            "compositeImage": {
                "id": "17a9d690-6ee1-4e6e-8e8b-700bf3579095",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23eef149-99f9-45b8-ad9e-23859d370142",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d34ca969-bcd6-435b-90a1-817a85240e8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23eef149-99f9-45b8-ad9e-23859d370142",
                    "LayerId": "f6f353d3-2999-4021-befc-b89b2f5d14fe"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f6f353d3-2999-4021-befc-b89b2f5d14fe",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "beed4201-d470-4875-8fa2-96d7b549223e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}