{
    "id": "22ff5d1b-eb31-423b-83cb-1eddc856ec50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sVectorTrail",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 4,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "f90c027d-0310-47ad-a5ba-542a0450f028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "22ff5d1b-eb31-423b-83cb-1eddc856ec50",
            "compositeImage": {
                "id": "432f89cb-79a6-4cde-8baf-4906834cae43",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f90c027d-0310-47ad-a5ba-542a0450f028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c5b736-5727-460d-87ca-ca3d23e4f1cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f90c027d-0310-47ad-a5ba-542a0450f028",
                    "LayerId": "66b5698b-b17d-4fad-b3fc-ae568f0b6174"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 6,
    "layers": [
        {
            "id": "66b5698b-b17d-4fad-b3fc-ae568f0b6174",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "22ff5d1b-eb31-423b-83cb-1eddc856ec50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 5,
    "xorig": 0,
    "yorig": 0
}