{
    "id": "6b8dbfc7-73b1-41d4-9721-8b762a010b54",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sVectorMove",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9e79ac81-c012-4366-85cb-450d211192a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b8dbfc7-73b1-41d4-9721-8b762a010b54",
            "compositeImage": {
                "id": "66c84461-5b7d-4f3f-94e8-3b4f4da38cdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e79ac81-c012-4366-85cb-450d211192a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab984600-b5e4-4679-ac15-d076fce54fea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e79ac81-c012-4366-85cb-450d211192a1",
                    "LayerId": "3e1b5993-c37e-4721-bf11-3ad68027d39d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "3e1b5993-c37e-4721-bf11-3ad68027d39d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b8dbfc7-73b1-41d4-9721-8b762a010b54",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 28,
    "xorig": 14,
    "yorig": 15
}