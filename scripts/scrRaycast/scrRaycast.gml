///@description RaycastEngine
///@param maxRay
///@param rayY
///@param rayDirection

var _ray=0;
var _maxRay=argument[0];
var _rayY=argument[1];
var _rayDir=argument[2];


while(_ray<_maxRay){
	if position_meeting(x,_rayY+(_rayDir*_ray),oBlock)
		break;
					
	_ray++;
}

return _ray;